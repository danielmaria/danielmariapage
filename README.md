[![angular-version](https://img.shields.io/badge/angular-v13.3.4-red)](https://angular.io/)

# Personal Web Page

This is a simple project that uses Angular to generate static documents and then, using Actions from GitHub, publishes the page that can be accessed in this [link](https://danielmaria.gitlab.io/danielmariapage).
